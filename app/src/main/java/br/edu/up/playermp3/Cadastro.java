package br.edu.up.playermp3;

import java.util.ArrayList;

public class Cadastro extends ArrayList<Musica> {

  //Método construtor
  //1. Não tem retorno;
  //2. Mesmo nome da classe;
  public Cadastro(){

    Musica m1 = new Musica();
    m1.setArtista("Anitta");
    m1.setNome("Bang");
    m1.setMp3(R.raw.anitta_bang);
    m1.setCapa(R.drawable.anitta);

    add(m1);


    Musica m2 = new Musica();
    m2.setNome("Na moral");
    m2.setMp3(R.raw.jota_quest_na_moral);
    m2.setCapa(R.drawable.jota_quest);
    m2.setArtista("Jota Quest");
    add(m2);

    Musica m3 = new Musica();
    m3.setNome("Another brick in te wall");
    m3.setMp3(R.raw.pink_floyd_another_brick_in_the_wall_p2);
    m3.setArtista("Pink Floyd");
    add(m3);

    Musica m4 = new Musica();
    m4.setNome("Sunday bloody sunday");
    m4.setMp3(R.raw.sambo_sunday_bloody_sunday);
    m4.setCapa(R.drawable.sambo);
    m4.setArtista("Sambô");
    add(m4);

    Musica m5 = new Musica();
    m5.setNome("Psycho killer");
    m5.setMp3(R.raw.talking_heads_psycho_killer);
    m5.setCapa(R.drawable.talking_heads);
    m5.setArtista("Taking Heads");
    add(m5);

    Musica m6 = new Musica();
    m6.setNome("Light my fire");
    m6.setMp3(R.raw.the_doors_light_my_fire);
    m6.setCapa(R.drawable.the_doors);
    m6.setArtista("The Doors");
    add(m6);

    Musica m7 = new Musica();
    m7.setNome("Disritmia");
    m7.setMp3(R.raw.zeca_baleiro_disritmia);
    m7.setCapa(R.drawable.zeca_baleiro);
    m7.setArtista("Zeca Baleiro");
    add(m7);

  }
}