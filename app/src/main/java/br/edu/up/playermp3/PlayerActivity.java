package br.edu.up.playermp3;


import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.ToggleButton;

import java.util.Random;

public class PlayerActivity extends AppCompatActivity
    implements MediaPlayer.OnCompletionListener,
    CompoundButton.OnCheckedChangeListener,
    View.OnTouchListener {

  private int posicao;
  private boolean tocarAleatorio = true;
  private SeekBar barraDeProgresso;
  private ImageButton btnPlay;
  private MediaPlayer player;
  private ImageView capa;
  private ToggleButton btnAleatorio;

  private Cadastro lista = new Cadastro();
  private Musica musicaAtual;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_player);

    capa = (ImageView) findViewById(R.id.imgCapa);
    barraDeProgresso = (SeekBar) findViewById(R.id.barraDeProgresso);
    btnPlay = (ImageButton) findViewById(R.id.btnPlay);
    btnAleatorio = (ToggleButton) findViewById(R.id.btnAleatorio);
    barraDeProgresso.setOnTouchListener(this);
    btnAleatorio.setOnCheckedChangeListener(this);

    Intent intent = getIntent();
    musicaAtual = (Musica) intent.getSerializableExtra("musicaAtual");

    tocarMusica();
  }

  public void tocarMP3(View v){
    if (player == null) {
      tocarMusica();
    } else if(player != null && !player.isPlaying()){
      player.start();
      btnPlay.setImageResource(R.drawable.pause50px);
    } else {
      player.pause();
      btnPlay.setImageResource(R.drawable.play50px);
    }
  }

  private void tocarMusica() {
    pararMusica();
    capa.setImageResource(musicaAtual.getCapa());
    btnPlay.setImageResource(R.drawable.pause50px);
    player = MediaPlayer.create(PlayerActivity.this, musicaAtual.getMp3());
    player.setOnCompletionListener(PlayerActivity.this);
    int duration = player.getDuration();
    barraDeProgresso.setMax(duration);
    player.start();
    atualizarProgresso();
  }

  public void atualizarProgresso(){
    if (player != null && player.isPlaying()) {
      int posicaoAtual = player.getCurrentPosition();
      barraDeProgresso.setProgress(posicaoAtual);
      Runnable processo = new Runnable() {
        @Override
        public void run() {
          atualizarProgresso();
        }
      };
      new Handler().postDelayed(processo, 1000);
    }
  }

  public void pararMP3(View v){
    pararMusica();
  }

  public void voltar(View v){
    selecionarMusica(-1);
    tocarMusica();
  }

  public void avancar(View v){
    selecionarMusica(1);
    tocarMusica();
  }

  @Override
  public void onCompletion(MediaPlayer mediaPlayer) {
    selecionarMusica(1);
    tocarMusica();
  }

  private void selecionarMusica(int incremento) {
    if(tocarAleatorio){
      sortearMusica();
    } else {
      posicao = posicao + incremento;
      if (posicao > lista.size() - 1) {
        posicao = 0;
      }
      musicaAtual = lista.get(posicao);
    }
  }

  private void sortearMusica() {
    Random r = new Random();
    posicao = r.nextInt(lista.size()-1);
    musicaAtual = lista.get(posicao);
  }

  private void pararMusica() {
    if (player != null) {
      btnPlay.setImageResource(R.drawable.play50px);
      player.stop();
      player.release();
      player = null;
    }
  }

  @Override
  public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
    tocarAleatorio = isChecked;
  }

  @Override
  public boolean onTouch(View view, MotionEvent motionEvent) {
    if (player != null) {
      SeekBar seekBar = (SeekBar) view;
      int progresso = seekBar.getProgress();
      player.seekTo(progresso);
    }
    return false;
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    pararMusica();
  }
}