package br.edu.up.playermp3;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class ListActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_list);

    final Cadastro cadastro = new Cadastro();
    MusicaAdapter adapter = new MusicaAdapter(this,cadastro);
    ListView listView = (ListView) findViewById(R.id.listView);
    listView.setAdapter(adapter);

    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Musica musica = cadastro.get(i);
        Intent intent = new Intent(ListActivity.this, PlayerActivity.class);
        intent.putExtra("musicaAtual", musica);
        startActivity(intent);
      }
    });

  }
}
