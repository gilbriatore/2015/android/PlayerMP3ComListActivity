package br.edu.up.playermp3;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class MusicaAdapter extends BaseAdapter {

  private Context context;
  private ArrayList<Musica> lista;

  public MusicaAdapter(Context context, ArrayList<Musica> lista){
    this.context = context;
    this.lista = lista;
  }

  @Override
  public int getCount() {
    return lista.size();
  }

  @Override
  public Object getItem(int i) {
    return lista.get(i);
  }

  @Override
  public long getItemId(int i) {
    return i;
  }

  @Override
  public View getView(int i, View view, ViewGroup viewGroup) {

    Musica musica = lista.get(i);

    LayoutInflater inflater = LayoutInflater.from(context);
    View item_musica = inflater.inflate(R.layout.item_musica, viewGroup, false);

    ImageView capinha = (ImageView) item_musica.findViewById(R.id.capinha);
    TextView txtMusica = (TextView) item_musica.findViewById(R.id.txtMusica);
    TextView txtArtista = (TextView) item_musica.findViewById(R.id.txtArtista);

    capinha.setImageResource(musica.getCapa());
    txtMusica.setText(musica.getNome());
    txtArtista.setText(musica.getArtista());

    return item_musica;
  }
}
