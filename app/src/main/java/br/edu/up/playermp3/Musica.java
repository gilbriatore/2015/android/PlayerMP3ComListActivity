package br.edu.up.playermp3;

import java.io.Serializable;

public class Musica implements Serializable {

  private int mp3;
  private int capa;
  private String nome;
  private String artista;

  public int getMp3() {
    return mp3;
  }

  public void setMp3(int mp3) {
    this.mp3 = mp3;
  }

  public int getCapa() {
    return capa;
  }

  public void setCapa(int capa) {
    this.capa = capa;
  }

  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getArtista() {
    return artista;
  }

  public void setArtista(String artista) {
    this.artista = artista;
  }
}